﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[DisallowMultipleComponent]
public class Oscillator : MonoBehaviour {
    // Start is called before the first frame update
    [SerializeField] Vector3 movementVector; //direction of movement
    [SerializeField] float period = 2f;
    //todo remove from inspector later
    [Range (0, 1)][SerializeField] float movementFactor;
    // 0 for not moved, 1 to fully moved
    private Vector3 startingPos;
    void Start () {
        startingPos = transform.position;
    }

    // Update is called once per frame
    void Update () {
        //todo  set movement factor automatically
        oscillate ();

    }
    public void oscillate () {
        if (period <= Mathf.Epsilon) { return; }
        float cycles = Time.time / period;

        const float tau = Mathf.PI * 2; // 2Pi

        //sin wave from calculating angle of time elapsed/set period*2pi
        float rawSinWave = Mathf.Sin (cycles * tau);
        movementFactor = rawSinWave / 2f + 0.5f;
        Vector3 offset = movementFactor * movementVector;
        transform.position = startingPos + offset;
    }
}