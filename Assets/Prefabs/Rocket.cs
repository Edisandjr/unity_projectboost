﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Rocket : MonoBehaviour {
    // Start is called before the first frame update
    Rigidbody rigidbody;
    AudioSource audioSource;
    KeyCode spaceButton = KeyCode.Space;
    [SerializeField] float rcsThrust = 650f;
    [SerializeField] float mainThrust = 300;
    [SerializeField] float levelLoadDelay = 2f;

    [SerializeField] AudioClip thrustClip;
    [SerializeField] AudioClip crashClip;
    [SerializeField] AudioClip successClip;


    [SerializeField] ParticleSystem mainEngineParticles;
    [SerializeField] ParticleSystem succesParticles;
    [SerializeField] ParticleSystem deathParticles;
    enum State { Alive, Dying, Transcending }
    State state = State.Alive;
    bool collisionOn = true;
    void Start () {
        rigidbody = GetComponent<Rigidbody> ();
        audioSource = GetComponent<AudioSource> ();
        audioSource.volume = 0.2f;
        audioSource.Stop ();
    }

    // Update is called once per frame
    void Update () {
        if (Debug.isDebugBuild){
            debugKeys();
        }
        if (state == State.Alive) {
            thrustResponse ();
            rotateShip ();
        }
    }
    void OnCollisionEnter (Collision collision) {
        if (state != State.Alive) { return; }
        switch (collision.gameObject.tag) {
            case "Friendly": //does nothing
                break;
            case "Terrain":
                {
                    if(collisionOn){
                        startDyingSequence ();
                    }
                    break;
                }
            case "Finish":
                {
                    startSuccessSequence ();
                    break;
                }
            default:
                break;
        }
    }
    private void thrustResponse () { //respond to thrust input
        if (Input.GetKey (spaceButton)) {
            thrustShip ();
        } else if (Input.GetKeyUp (spaceButton)) {
            audioSource.Stop ();
            mainEngineParticles.Stop ();
        }
        //thrust forward
    }
    private void thrustShip () {
        rigidbody.AddRelativeForce (Vector3.up * mainThrust * Time.deltaTime);
        if (!audioSource.isPlaying) {
            audioSource.PlayOneShot (thrustClip);
        }
        mainEngineParticles.Play ();
    }
    private void rotateShip () {
        rigidbody.freezeRotation = true; // take manual control of rotation
        float rotationThisFrame = rcsThrust * Time.deltaTime;
        if (Input.GetKey (KeyCode.A)) {
            transform.Rotate (Vector3.forward * rotationThisFrame);
            //rotate left
        } else if (Input.GetKey (KeyCode.D)) {
            transform.Rotate (-Vector3.forward * rotationThisFrame);
            //rotate right
        }
        rigidbody.freezeRotation = false; // resume phyiscs rocket
    }
    private void startDyingSequence () {
        state = State.Dying;
        audioSource.Stop ();
        deathParticles.Play ();
        audioSource.PlayOneShot (crashClip, .5F);
        //yield return new WaitForSeconds(4F);
        Invoke ("restart", levelLoadDelay);
    }
    private void startSuccessSequence () {
        state = State.Transcending;
        audioSource.Stop ();
        succesParticles.Play ();
        audioSource.PlayOneShot (successClip, .5f);
        Invoke ("loadNextLevel", levelLoadDelay);
    }
    private void restart () {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
      /* if (currentSceneIndex != 0)
            SceneManager.LoadScene(--currentSceneIndex);
        else
        -- hard mode
        */
            SceneManager.LoadScene(currentSceneIndex);
    }
    private void loadNextLevel () {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(++currentSceneIndex);
    }
    private void stopControls () { //Deprecated
        rcsThrust = 0f;
        mainThrust = 0f;
        spaceButton = KeyCode.None;
    }
    // debug keys
    private void debugKeys(){
        if(Input.GetKey(KeyCode.L)){
             loadNextLevel();
        }
        if(Input.GetKey(KeyCode.C)){
            collisionOn = !collisionOn;
        }
    }
}